window.onload=()=>{
    function RiQi(sj)
    {
        let now = sj;
        let year=now.getFullYear();
        let month=now.getMonth()+1;
        let date=now.getDate();
        let hour=now.getHours();
        let minute=now.getMinutes();
        let second=now.getSeconds();
        return year+"-"+month+"-"+date+" "+hour+":"+minute+":"+second;

    }
    function msg(msg){
        $("#info").html(msg).fadeIn(200);
        setTimeout(()=>{
            $("#info").fadeOut(200);
        },2000)
    }
    laydate.render({
        elem: '#StartTime' //指定元素
        ,type:"datetime"
        ,value:localStorage["StartTime"]
    });
    //document.querySelector("#StartTime").value=new Date();
    let dom=document.querySelector("#Save");
    dom.onclick=function(){
        let tim=document.querySelector("#StartTime").value;
        if(tim){
            localStorage["StartTime"]=tim;
            msg("设置成功");
        }else{
            msg("请输入正确的时间");
        }
    };
};