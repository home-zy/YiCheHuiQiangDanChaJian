/**
 * 作者：易支烟
 * QQ：464312406
 * 博客：www.1zyan.cn
 * 更新时间：2019-6-14
 * 版本：0.1.3
 */
(function(exports){
    //popup
    try{
        exports.bg=chrome.extension.getBackgroundPage();
    }catch (e) {}
    function Base(){
        let thi={};
        thi.getUrl=function(url){
            return chrome.extension.getURL(url);
        };
        thi.loadClass=function(url){
            let doc=thi.toDocument('<link rel="stylesheet" href="'+url+'">');
            (document.head||document.documentElement).appendChild(doc);
        };
        thi.loadStyle=function(css){
            let doc=thi.toDocument('<style>'+css+'</style>');
            (document.head||document.documentElement).appendChild(doc);
        };
        thi.loadScript=function(url, callback) {
            let script = document.createElement("script");
            script.type = "text/javascript";
            if (script.readyState) {
                script.onreadystatechange = function () {
                    if (script.readyState == "loaded" || script.readyState == "complete") {
                        script.onreadystatechange = null;
                        if(callback)callback();
                    }
                };
            } else {
                script.onload = function () {
                    if(callback)callback();
                };
            }
            script.src = url;
            (document.head||document.documentElement).appendChild(script);
        };
        thi.toDocument=function(html){
            let ht=document.createElement('div');
            ht.innerHTML=html;
            return ht.firstElementChild;
        };
        thi.getScriptList=function(){
            let list=document.querySelectorAll('script');
            let linkArr=[];
            let scriptArr=[];
            list.forEach((e)=>{
                if(e.src){
                    linkArr.push(e.src);
                }else if(e.innerHTML.trim()){
                    scriptArr.push(e.innerHTML.trim())
                }
            });
            return {linkArr,scriptArr}
        };
        thi.Ajax={
            toPar:function(data){
                if(typeof data==='object'){
                    let ret=null;
                    let str=null;
                    Object.keys(data).forEach(function(e){
                        str=encodeURIComponent((typeof data[e]==='string'?data[e]:JSON.stringify(data[e])));
                        if(ret===null){
                            ret=e+"="+str;
                        }else{
                            ret+="&"+e+"="+str;
                        }
                    });
                    return ret;
                }else{
                    return data;
                }
            },
            get: function(url, fn,error) {
                // XMLHttpRequest对象用于在后台与服务器交换数据
                var xhr = new XMLHttpRequest();
                xhr.open('GET', url, false);
                xhr.onreadystatechange = function() {
                    // readyState == 4说明请求已完成
                    if (xhr.readyState == 4 && xhr.status == 200 || xhr.status == 304) {
                        // 从服务器获得数据
                        fn.call(this, xhr.responseText);
                    }else{
                        if(error)error.call(this,xhr)
                    }
                };
                xhr.send();
            },
            post: function (url, data, fn,error) {
                var xhr = new XMLHttpRequest();
                xhr.open("POST", url, false);
                // 添加http头，发送信息至服务器时内容编码类型
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xhr.onreadystatechange = function() {
                    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 304)) {
                        fn.call(this, xhr.responseText);
                    }else{
                        if(error)error.call(this,xhr)
                    }
                };
                xhr.send(this.toPar(data));
            },
            getJSON:function(url, fn,error){
                this.get(url,function (e) {
                    fn.call(this,eval('('+e+')'))
                },error)
            },
            postJSON:function(url,data, fn,error){
                this.post(url,data,function (e) {
                    fn.call(this,eval('('+e+')'))
                },error)
            }
        };
        thi.removeElement=function(dom){
            dom.parentElement.removeChild(dom);
        };
        thi.addHandler= function (element, type, handler) {

            if (element.addEventListener) {

                element.addEventListener(type, handler, false);

            } else if (element.attachEvent) {

                element.attachEvent("on" + type, handler);

            } else {

                element["on" + type] = handler;

            }

        };
        thi.preventDefault=function (event) {
            if (event.preventDefault) {
                event.preventDefault();
            } else {
                event.returnValue = false;
            }
        };
        thi.hook_fn=function(obj,hookName,callback){
            if(!(obj && hookName && callback)){
                return false;
            }
            if(!thi.hooks){
                thi.hooks={};
            }
            if(typeof hookName==="string" && !thi.hooks[hookName]){
                thi.hooks[hookName]=obj[hookName];
                obj[hookName]=function (a,b,c,d,e,f,g) {
                    if(callback){
                        if(callback.call(this,a,b,c,d,e,f,g)===false){
                            return;
                        }
                    }
                    thi.hooks[hookName].call(this,a,b,c,d,e,f,g);
                };
                return true;
            }
            return false;
        };
        return thi;
    }
    //background
    function Background(){
        let thi=Base();
        //监听消息
        thi.on=function(callback){
            let callbackBase=callback;
            chrome.runtime.onMessage.addListener(function (request,sender,callback) {
                if(callbackBase(request,sender,callback)===undefined){
                    return true;
                }
            });
        };
        //发送消息
        thi.sendTabInfo=function (tabId,sendInfo,response) {
            if(tabId){
                thi.getTabInfo(tabId,function(tab){
                    if(tab){
                        if(tab.length && tab.length>0){
                            for (let i=0;i<tab.length;i++){
                                chrome.tabs.sendMessage(tab[i].id, sendInfo, response);
                            }
                        }else{
                            chrome.tabs.sendMessage(tab.id, sendInfo, response);
                        }
                    }
                });
            }else{
                //给当前打开窗口发送信息
                chrome.tabs.query({
                    active:true,
                    currentWindow:true
                },function(e){
                    chrome.tabs.sendMessage(e[0].id, sendInfo, response);
                })
            }
        };
        //查询当前地址
        thi.sendInfo=function(info,response){
            chrome.runtime.sendMessage(info,response);
        };
        //获取tab信息
        thi.getTabInfo=function(tabId,callback){
            try{
                if(typeof tabId==="number"){
                    chrome.tabs.get(tabId,function(e) {
                        if(callback)return callback.call(this,e);
                    });
                }else if(typeof tabId==="string"){
                    chrome.tabs.query({
                        url:tabId
                    },function(e){
                        if(callback)return callback.call(this,e);
                    });
                }else if(typeof tabId==="object"){
                    chrome.tabs.query(tabId,function(e){
                        if(callback)return callback.call(this,e);
                    });
                }else{
                    //获取所有
                    chrome.tabs.query({},function(e){
                        if(callback)return callback.call(this,e);
                    });
                }
            }catch (e) {
                if(callback)return callback.call(this,null);
            }
        };
        //获取tab状态 false刷新中 true加载完成
        thi.getTabStatus=function(tabId,callback){
            thi.getTabInfo(tabId,function(e){
                if(callback)callback.call(this,(!e)?null:e.status==="loading"?false:true);
            })
        };
        //创建一个tab
        thi.createTab=function(obj,callback){
            /*
            整数	（可选） windowId
            用于创建新选项卡的窗口。默认为当前窗口。

            整数	（可选） 索引
            选项卡应在窗口中占据的位置。提供的值被限制在零和窗口中的选项卡数之间。

            串	（可选） 网址
            最初将选项卡导航到的URL。完全限定的网址必须包含一个方案（即'http://www.google.com'，而不是'www.google.com'）。相对URL相对于扩展中的当前页面。默认为新标签页。

            布尔	（可选） 有效
            选项卡是否应成为窗口中的活动选项卡。不影响窗口是否聚焦（请参阅windows.update）。默认为true。

            布尔	（可选）已 选中
            自Chrome 33以来已弃用。请使用有效。

            选项卡是否应成为窗口中的选定选项卡。默认为true

            布尔	（可选） 固定
            是否应固定标签。默认为false

            整数	（可选） openerTabId
            打开此选项卡的选项卡的ID。如果指定，则opener选项卡必须与新创建的选项卡位于同一窗口中。
             */
            return chrome.tabs.create(obj,callback);
        };
        //复制密码
        thi.copyTab=function(tabId,callback){
            chrome.tabs.duplicate(tabId,callback);
        };
        //发送桌面通知
        let win={};
        //直接向桌面发送一个文本消息
        win.sendInfo=function(icon,title,content){
            let notification = webkitNotifications.createNotification(icon,title,content);
            notification.show();
        };
        //直接向桌面发送一个HTML消息
        win.sendHtml=function(htmlUrl){
            let notification = webkitNotifications.createHTMLNotification(htmlUrl);
            notification.show();
        };
        thi.win=win;
        //web操作模块
        let web={};
        //监听发送之前request
        web.beforeRequest=function(callback){
            chrome.webRequest.onBeforeRequest.addListener(callback)
        };
        this.__proto__=thi;
    }
    //content_script
    function Content_script(){
        let thi=Base();
        //注入方式运行Js
        thi.runJs=function(js){
            let script=document.createElement("script");
            script.innerHTML=js;
            (document.head||document.documentElement).appendChild(script);
        };
        //Eval方式运行JS
        thi.runJsEval=function(js){
            eval(js);
            let id='zy_temp_'+new Date().getTime();
            let html=`<img src='' id='${id}' onclick='eval(\`${js}\`)'/>`;
            $('body').append(html);
            id=$('#'+id);
            id.click();
            id.remove();
        };
        //直接运行
        thi.runThisJs=function(js){
            eval(js)
        };
        //监听消息
        thi.on=function(callback){
            let callbackBase=callback;
            chrome.runtime.onMessage.addListener(function (request,sender,callback) {
                if(callbackBase(request,sender,callback)===undefined){
                    return true;
                }
            });
        };
        //像背景页发送信息
        thi.sendBgInfo=function(info,callback){
            try{
                if(callback){
                    return chrome.runtime.sendMessage(info,callback);
                }else{
                    return chrome.runtime.sendMessage(info);
                }
            }catch (e) {
                
            }
        };
        //截图可视区域
        thi.picture=function(windowId,callback,isJpg){
            let type={
                format:"png"
            };
            if(isJpg){
                type.format="jpeg";
            }
            //callback 参数一：图片数据可以使用img标签显示出来
            chrome.tabs.captureVisibleTab(windowId, type ,callback);
        };
        this.__proto__=thi;
    }
    //导出
    exports.background=Background;
    exports.content_script=Content_script;
})(typeof exports === "object" && exports || this);